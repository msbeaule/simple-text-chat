class UsernameValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /^[A-Za-z0-9-_]{3,50}$/
      record.errors[attribute] << (options[:message] || "is not a valid username. (Can only contain letters, numbers, dashes - and underscores _)")
    end
  end
end


class User < ApplicationRecord
  has_many :messages
  has_many :chatrooms, through: :messages

  validates :username, presence: true, length: { maximum: 50, minimum: 3 }, uniqueness: { case_sensitive: false }, username: true
  validates :email, presence: true, length: { maximum: 255 }, uniqueness: { case_sensitive: false }

  has_secure_password

  before_save do
    self.email = email.downcase
  end
end
