class SessionsController < ApplicationController

  def new_signup
  end

  def new_login
  end

  def create_for_existing_user
    user = User.find_by(email: params[:session][:email].downcase)

    if user && user.authenticate(params[:session][:password])
      # Handle the logic for after a user is authenticated
      helpers.log_in(user)
      redirect_to root_url
    else
      flash.now[:danger] = 'Invalid email or password.'
      render 'new_login'
    end
  end

  def create_for_new_user
    @user = User.create(username: params[:session][:username], email: params[:session][:email], password: params[:session][:password])

    if @user.errors.count < 1
      helpers.log_in(@user)
      flash[:success] = "New account created!"
      redirect_to root_url
    else
      render 'new_signup'
    end

    # if user
    #   user.save
    #   flash.now[:success] = 'Account created!'
    #   redirect_to root_url
    # end
  end

  def destroy
    helpers.log_out
    redirect_to root_url
  end

end
