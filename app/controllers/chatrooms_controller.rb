class ChatroomsController < ApplicationController
  before_action :check_if_logged_in

  def index
    @chatroom = Chatroom.new
    @chatrooms = Chatroom.all.order('topic asc')
  end

  def new
    @chatroom = Chatroom.new
  end

  def create
    @chatroom = Chatroom.new(chatroom_params)
    if @chatroom.save
      respond_to do |format|
        format.html { redirect_to @chatroom }
        format.js
      end
    else
      respond_to do |format|
        flash[:notice] = {error: ["a chatroom with this topic already exists"]}
        format.html { redirect_to new_chatroom_path }
        format.js { render template: 'chatrooms/chatroom_error.js.erb'} 
      end
    end
  end

  def show
    @chatroom = Chatroom.find_by(slug: params[:slug])
    @message = Message.new
  end

  private

    def chatroom_params
      params.require(:chatroom).permit(:topic)
    end

    def check_if_logged_in
      if !logged_in?
        redirect_to root_path
        flash[:notice] = "Please login before going to a chatroom."
      end
    end

end
