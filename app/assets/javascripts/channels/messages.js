App.messages = App.cable.subscriptions.create('MessagesChannel', {  
  received: function(data) {
    $("#messages").removeClass('hidden')
    return $('#messages').append(this.renderMessage(data));
  },

  renderMessage: function(data) {
    return "<p> <strong> <a href=../u/" + data.user + ">" + data.user + "</a>: </strong>" + data.message + "</p>";
  }
});