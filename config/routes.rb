Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/', to: "index#index"
  get '/signup', to: "sessions#new_signup"
  post '/signup', to: "sessions#create_for_new_user"
  get '/login', to: "sessions#new_login"
  post '/login', to: "sessions#create_for_existing_user"
  delete '/logout', to: "sessions#destroy"

  resources :u, controller: "users", only: [:show], param: :username

  resources :chatrooms, param: :slug
  resources :messages

  root 'index#index'

  mount ActionCable.server => '/cable'
end
